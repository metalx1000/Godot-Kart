extends RigidBody

var snd = 0

func _ready():
	pass # Replace with function body.

func _on_Area_body_entered(body):
	if snd != 0:
		return
		
	snd = 1
	$sound_reset.start()
	
	if rand_range(0,3) > 2:
		$crash.play()

func _on_sound_reset_timeout():
	snd = 0
