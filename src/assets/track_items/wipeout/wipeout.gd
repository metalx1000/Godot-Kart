extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



func _on_wipeout_body_entered(body):
	#print("Collision")
	var parent = body.get_parent()
	if "wiped" in parent:
		#print("wipeout")
		randomize()
		parent.wiped_v = rand_range(-2,2)
		parent.wiped = 2
