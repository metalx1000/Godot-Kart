extends Control

func _ready():
	$AnimationPlayer.stop(true)
	$AnimationPlayer.play("intro")
	
func _input(event):
	if event.is_action_pressed("ui_accept"):
		next()
		
func next():
	get_tree().change_scene("res://tracks/test_track_2/TestScene.tscn")
