extends Spatial
onready var paths = [$track/Path,$track/Path2]

func get_path_direction(position,path_num):
	var path = $track/paths.get_child(path_num)
	var offset = path.curve.get_closest_offset(position)
	var pathfollow = path.get_child(0)
	pathfollow.offset = offset
	return pathfollow.transform.basis.z

func _ready():
	print($track["transform"])
