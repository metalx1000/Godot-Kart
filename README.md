#Godot-Kart

Copyright Kris Occhipinti 2022-06-24
(https://filmsbykris.com)
License GPLv3


#original code
The base of this Game is based on the example project:
-- Demo using a rolling sphere to simulate car physics.
Tutorial: http://kidscancode.org/godot_recipes/3d/3d_sphere_car/

Original Project was under an MIT License
All updates added in this project are under a License GPLv3

